export const ROUTE_BASE_PATH = '/fed/admin/api';

export const REDIS_CONF = {
    host:'116.62.188.71',
    port:6379,
    password:'dmcfed321',
    connect_timeout:3600000,
}

//本地模拟测试
 export const SERVER_BASE_PATH = 'http://localhost:9900/fed/admin/api/mock';

//生产环境要放开
//  export const WECHAT_BASE_PATH = 'https://qyapi.weixin.qq.com/cgi-bin';
// export const SERVER_BASE_PATH = 'http://carowner.yonyouauto.com/qy';

//直接连微服务
  //export const SERVER_BASE_PATH = 'http://localhost:9020';

// 定时器时间间隔初始值
export const SCHEDULE_TIME =  {
    once: 1,
    many: [1,6,11,16,21,26,31,36,41,46,51,56]
};  