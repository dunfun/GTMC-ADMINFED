//模拟数据
import moment from 'moment';
import BaseRoute from '../../baseRoute';
import { ROUTE_BASE_PATH } from '../../config';
import uuid from 'uuid';
import AuthUtil from '../../authUtil';
class DemoList extends BaseRoute {
  handle() {
    const serverTime = moment().format('YYYY-MM-DD HH:mm:ss');
    const model = {
      "resultCode": 1,
      "data": {
        "pageNum": 1,
        "pageSize": 10,
        "size": 1,
        "orderBy": null,
        "startRow": 1,
        "endRow": 1,
        "total": 100,
        "pages": 1,
        "list": [],
        "firstPage": 1,
        "prePage": 0,
        "nextPage": 0,
        "lastPage": 1,
        "isFirstPage": true,
        "isLastPage": false,
        "hasPreviousPage": false,
        "hasNextPage": false,
        "navigatePages": 8,
        "navigatepageNums": [1]
      },
      "errMsg": "mock数据",
      "time": serverTime,
      "elapsedMilliseconds": 0,
      "success": true
    }

    const row = {
      "uid": 20,
      "sex": null,
      "licenseNo":"沪AK998R",
      "userName": "陆渐离",
      "userPhone": "13156671556",
      "address": "上海市长宁区延安西路1100号辽油大厦12楼",
      "applyTime": serverTime,
    }


    let pageNum = this.req.query.pageNum;
    let pageSize = this.req.query.pageSize;

      pageNum = pageNum?pageNum:1;
      pageSize = pageSize?pageSize:10;
     model['data']['pageSize'] = pageSize;
     const usrs = ['陆渐离','童蕾','陈独秀','欧阳明月','黄逸仙','李维斯'];
     
console.log('pageNum===',pageNum)
    for(let i=1,len=pageSize;i<=pageSize;i++){
      const randomIndex = Math.ceil(Math.random()*5);
      const item = {
        uid: 'NO.'+(pageNum-1+i)*100000,
        sex: i%2 === 0?'先生':'女士',
        userName: usrs[randomIndex]
      }
      model['data']['list'].push({...row,...item});
    }






    this.res.json(model);

  }
}




const routeHandle = (router) => {

  router.get(`${ROUTE_BASE_PATH}/mock/demo/list`, DemoList.makeRouteHandle());

};

export default routeHandle;