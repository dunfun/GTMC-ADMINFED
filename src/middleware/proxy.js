// 代理路由转发
import express from 'express';
import path from 'path';
import {ROUTE_BASE_PATH} from './config';
import demoMock from './handlers/mockHandler/demo';
import pmHandler from './handlers/pmHandler';
import healthHandler from './handlers/healthHandler';
import gatewayHandler from './handlers/gatewayHandler';
const options = {'caseSensitive': true, 'strict': true};
const proxyDispatcher = express.Router(options);


demoMock(proxyDispatcher);
pmHandler(proxyDispatcher);
healthHandler(proxyDispatcher);

gatewayHandler(proxyDispatcher);

proxyDispatcher.use(`${ROUTE_BASE_PATH}/*`,proxyDispatcher,(err, req, res, next) => {

    if (err instanceof URIError) {
        return next();
    }

    throw err;
});

export default proxyDispatcher;

