/**
 *redux
 */

import React from 'react';
import Layout from '../../components/Layout';
import Http from '../../utils/http'
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { setTimeout } from 'timers';
const title = 'redux';

const apis = [
  {"id":"driverInfo","url":"dealer/api/v1/driverInfo"},
];

Http.setDomainUrl("/wx/ent/api/");

Http.setMutiApi(apis);


class ReduxIndex extends React.Component {
  constructor(props) {
    super(props);

    const userInfo = Object.assign({},props['user']);

    this.state = {
      driveId:'',
      driverInfo:{

      }
    };

    this.state.driveId = userInfo['driveId'];

  }

  componentDidMount() {
    const that = this;

    const test1 = {a:1,b:2,c:3};
    const test2 = {a:9,d:6};
    const otest = {...test1,...test2};

    let arr1 = [1,2,3];
    let arr2 = [...arr1,4,5,6];
    let arr = [...arr1,...arr2];
    console.log('otest====',otest);
    console.log('arr2====',arr2);

    function say(count){
        console.log('i===',count);
    }

    for(let i=0,len=3;i<=len;i++){
        let timer = setTimeout(function(){
            return say(i);
            
        },i*1500)
    }

    Http.get('driverInfo',{driveId:this.state.driveId},function(data){

      that.setState({
        driverInfo:data
      })

    })
  }

 
  render() {
    const that = this;
  
   
    return (
      <div style={{width:'100%'}}>
            <h5>redux</h5>

      </div>

    );
  }
 
}

const ReduxComp = withStyles()(ReduxIndex);

function action({path, query, hash}) {
  const userInfo = query;
  return {
    chunks: ['redux'],
    title,
    component: (
      <Layout  hide={true}>
        <ReduxComp user={userInfo} />
      </Layout>
    ),
  };
}

export default action;
