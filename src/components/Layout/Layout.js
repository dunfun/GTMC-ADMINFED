/**
 * 全局容器
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { LocaleProvider } from 'antd';
// external-global styles must be imported in your JS.
import base from './style/base.scss';
import s from './Layout.css';
import Header from '../Header';
import Feedback from '../Feedback';
import Footer from '../Footer';
import zhCN from 'antd/lib/locale-provider/zh_CN';

class Layout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  
 

  render() {
    const style = {
      display:!this.props['hide']?'block':'none'
    }
    return (
      <LocaleProvider locale={zhCN}>
      <div>
          {this.props.children}
          <div style={style}>
          <Footer />
          </div>
      </div>
      </LocaleProvider>
      
    );
  }
}

// export default withStyles(normalizeCss, antdCss, s, base)(Layout);
export default withStyles(s, base)(Layout);
